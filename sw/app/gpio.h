
/*
  Copyright (C) 2021 Jonathan Certes

  This program is free software: you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free Software
  Foundation, either version 3 of the License, or (at your option) any later
  version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __GPIO_H
  #define __GPIO_H

  #include <stdint.h>

  /**
   * Base address of XGPIOPS registers
   */
  #define XGPIOPS_BASE_ADDR    0xE000A000U

  /**
   * Offset for data register
   */
  #define XGPIOPS_DATA_OFFSET  0x00000040U
  /**
   * Offset for direction mode register
   */
  #define XGPIOPS_DIRM_OFFSET  0x00000204U
  /**
   * Offset for output enable register
   */
  #define XGPIOPS_OUTEN_OFFSET 0x00000208U
  /**
   * Offset for interrupt enable/unmask
   */
  #define XGPIOPS_INTEN_OFFSET 0x00000210U

  #define GPIO_DIRECTION_INPUT  0 //!< Direction for \ref gpio_setDirection()
  #define GPIO_DIRECTION_OUTPUT 1 //!< Direction for \ref gpio_setDirection()

/******************************************************************************/

  void gpio_setDirection(uint8_t theGpioIndex, uint8_t theDirection);
  void gpio_setOuputEnable(uint8_t theGpioIndex, uint8_t theEnable);
  uint32_t gpio_read(uint8_t theGpioIndex);
  void gpio_write(uint8_t theGpioIndex, uint8_t theValue);

#endif
