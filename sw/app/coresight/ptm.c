/**
 * \file  ptm.c
 */

/*
  Copyright (C) 2021 Jonathan Certes

  This program is free software: you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free Software
  Foundation, either version 3 of the License, or (at your option) any later
  version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "ptm.h"

/******************************************************************************/

/**
 * \defgroup ptm  Program Trace Macrocell (ptm)
 *
 * \brief
 *  Library to manipulate CoreSight Program Trace Macrocell (ptm) under Xilinx
 *  Zynq-7000.
 *
 * \{
 */

/**
 * \brief
 *  Structure to access CoreSight Program Trace Macrocell (ptm) registers for
 *  CPU0.
 */
struct ptm* ptm_cpu0 = (struct ptm*)( PTM_BASE_CPU0 );

/**
 * \brief
 *  Structure to access CoreSight Program Trace Macrocell (ptm) registers for
 *  CPU1.
 */
struct ptm* ptm_cpu1 = (struct ptm*)( PTM_BASE_CPU1 );

/******************************************************************************/

/**
 * \brief
 *  Initialize the PTM to activate the traces in a given address range.
 */
void ptm_init(
  uint8_t  theCpuIndex,     //!< index of the CPU to activate the traces of
  uint32_t traceRangeStart, //!< trace range start address
  uint32_t traceRangeStop   //!< trace range stop address
) {
  struct ptm* thePtm;
  switch ( theCpuIndex ) {
    case 1:
      thePtm = ptm_cpu1;
      break;
    default:
      thePtm = ptm_cpu0;
      break;
  }

  thePtm->lar = 0xC5ACCE55;
  thePtm->etmcr.ProgBit = 1;
  while ( thePtm->etmsr.ProgBit != 1 ) {
    // wait
  }

  // Main Control Register:
  thePtm->etmcr.BranchOutput   = 1;     // addresses are output
  thePtm->etmcr.DebugReqCtrl   = 0;     // not in debug mode
  thePtm->etmcr.PowerDown      = 0;     // enable PTM
  // TraceEnable Event Register:
  thePtm->etmteevr.Function    = 0x0;   // logical operation = A
  thePtm->etmteevr.ResourceA   = (0x1 << 4) | (0x0 << 0);  // Address range comparators 1
  thePtm->etmteevr.ResourceB   = 0x00;  // don't care
  // Trigger Event Register:
  thePtm->etmtrigger.Function  = 0x0;   // logical operation = A
  thePtm->etmtrigger.ResourceA = (0x1 << 4) | (0x0 << 0);  // Address range comparators 1
  thePtm->etmtrigger.ResourceB = 0x00;  // don't care
  // TraceEnable Control Register 1:
  thePtm->etmtecr1.TraceSSEn   = 0;     // unaffected by TraceEnable start/stop block
  thePtm->etmtecr1.ExcIncFlag  = 1;     // exclude
  thePtm->etmtecr1.AddrCompSel = 0x0;   // no address range
  // CoreSight Trace ID Register:
  thePtm->etmtraceidr.TraceID  = 0x42;  // non-zero, not in range 0x70-0x7F
  // Address Comparator registers:
  thePtm->etmacvr1.Address     = traceRangeStart;
  thePtm->etmacvr2.Address     = traceRangeStop;

  thePtm->etmcr.ProgBit = 0;
  while ( thePtm->etmsr.ProgBit != 0 ) {
    // wait
  }
  thePtm->lar = 0x00000000;
}

/******************************************************************************/

/**
 * \}
 */

