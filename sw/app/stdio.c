
/*
  Copyright (C) 2021 Jonathan Certes

  This program is free software: you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free Software
  Foundation, either version 3 of the License, or (at your option) any later
  version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <xuartps_hw.h>
#include <sys/stat.h>

/**
 * \defgroup system   System calls
 *
 * \brief
 *  Overloading system calls so that we can call <tt> scanf() </tt> and <tt>
 *  printf() </tt> and make them read/write on UART 0.
 *
 * \details
 *  Works when linking with arm-none-eabi newlib.
 *
 * \{
 */

#define UART0_BASE 0xE0000000U //!< Base address of UART0 registers
#define UART1_BASE 0xE0001000U //!< Base address of UART1 registers

/**
 * This symbol is defined in the linker script: beginning of the heap.
 * Useful for function \ref _sbrk() called by <tt> printf() </tt>.
 */
extern int __heap_low;

/******************************************************************************/
/******************************************************************************/

/**
 * Returns the status of an open file. The minimal version of this should
 * identify all files as character special devices. This forces one-byte-read at
 * a time.
 */
int _fstat(int file, struct stat *st) {
  st->st_mode = S_IFCHR;
  return 0;
}

/******************************************************************************/

/**
 * Repositions the file offset of the open file associated with the file
 * descriptor fd to the argument offset according to the directive whence.
 * Here we can simply return 0, which implies the file is empty.
 */
int _lseek(int file, int offset, int whence) {
  return 0;
}

/******************************************************************************/

/**
 * Closes a file descriptor fd.
 * Since no file should have gotten open()-ed, we can just return an error on
 * close.
 */
int _close(int fd) {
  return -1;
}

/******************************************************************************/

/**
 * Writes up to count bytes from the buffer starting at buf to the file referred
 * to by the file descriptor fd.
 * Functions like printf() rely on write to write bytes to STDOUT. In our case,
 * we will want those bytes to be written to serial instead.
 */
int _write(
  int fd,
  const void *buf,
  size_t count
) {
  char theChar;
  size_t i;

  (void)fd; // Parameter is not used, suppresses unused argument warning

  for ( i = 0; i < count; i++ ) {
    theChar = *(uint8_t*)buf;
    if ( theChar == '\n' ) {
      XUartPs_SendByte(UART0_BASE, '\r'); // UART0 is connected to USB on CoraZ7
    }
    XUartPs_SendByte(UART0_BASE, theChar);
    (uint8_t*)buf++;
  }

  return i;
}

/******************************************************************************/

/**
 * Attempts to read up to count bytes from file descriptor fd into the buffer at
 * buf.
 * Similarly to write, we want read to read bytes from serial.
 */
int _read(
  int fd,
  char *buf,
  int count
) {
  int read = 0;

  while (!XUartPs_IsReceiveData(UART0_BASE)) {
    // do nothing
  }

  for ( int i = 0; i < count; i++ ) {

    if ( XUartPs_IsReceiveData(UART0_BASE) ) {
      *buf = (char)( XUartPs_ReadReg(UART0_BASE, XUARTPS_FIFO_OFFSET) );
      buf++;
      read++;
    }

  }

  return read;
}

/******************************************************************************/

/**
 * Increases the program’s data space by increment bytes. In other words, it
 * increases the size of the heap.
 * Newlib's printf implementations allocates data on the heap and depends on a
 * working malloc implementation.
 */
void * _sbrk(int incr) {
  static unsigned char *heap = NULL;
  unsigned char *prev_heap;

  if (heap == NULL) {
    heap = (unsigned char *)&__heap_low;
  }
  prev_heap = heap;

  heap += incr;

  return prev_heap;
}

/******************************************************************************/

/**
 * Always tell <tt> printf() </tt> that its printing into a tty, which has
 * direct connexion to input/output peripherals.
 */
int _isatty(int file) {
  return 1;
}

/******************************************************************************/

/**
 * \}
 */
