
/*
  Copyright (C) 2021 Jonathan Certes

  This program is free software: you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free Software
  Foundation, either version 3 of the License, or (at your option) any later
  version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <stdio.h>

#include "xil_mmu.h"
#include "xil_cache.h"

#include "gpio.h"
#include "coresight/ptm.h"
#include "coresight/funnel.h"
#include "coresight/etb.h"

/**
 * Location where to copy the code and reconfigure the MMU.
 */
#define ATTACK_ADDRESS 0x00200000

/**
 * size of one page when we reconfigure the MMU.
 */
#define MMU_PAGE_SIZE (1 << 20)

/**
 * \brief
 *  Integer that refers to the output pin index: <tt> EMIO[0] </tt>. See details
 *  of \ref gpio_read().
 */
#define GPIO_LOGIC_ANALYSER_RESET 54

/** symbols that are defined in the linker script. **/
extern int __sw_att_low;
extern int __sw_att_top;
//
extern int __logica_low;
extern int __logica_top;

/** symbols that are defined in this file with in-line assembly. **/
extern int __attack_readAndBranch1_start, __attack_readAndBranch1_stop;
extern int __attack_readAndBranch2_start, __attack_readAndBranch2_stop;
extern int __attack_readAndBranch3_start, __attack_readAndBranch3_stop;

/******************************************************************************/

void configureCoresight(
  uint32_t *theStartAddress,
  uint32_t *theStopAddress
) {
  /********************/
  /** configure PTM: **/
  /********************/
  ptm_cpu0->lar = 0xC5ACCE55;
  ptm_cpu0->etmcr.ProgBit = 1;
  while ( ptm_cpu0->etmsr.ProgBit != 1 ) {
    // wait
  }
  // Main Control Register:
  ptm_cpu0->etmcr.BranchOutput   = 0;     // direct branch addresses not output
  ptm_cpu0->etmcr.DebugReqCtrl   = 0;     // not in debug mode
  ptm_cpu0->etmcr.PowerDown      = 0;     // enable PTM
  ptm_cpu0->etmcr.CycleAccurate  = 1;
  ptm_cpu0->etmcr.ContexIDSize   = 3;     // context id size = 32 bits
  // TraceEnable Event Register:
  ptm_cpu0->etmteevr.Function    = 0x0;   // logical operation = A
  ptm_cpu0->etmteevr.ResourceA   = (0x1 << 4) | (0x0 << 0);  // Address range comparators 1
  ptm_cpu0->etmteevr.ResourceB   = 0x00;  // don't care
  // Trigger Event Register:
  ptm_cpu0->etmtrigger.Function  = 0x0;   // logical operation = A
  ptm_cpu0->etmtrigger.ResourceA = (0x1 << 4) | (0x0 << 0);  // Address range comparators 1
  ptm_cpu0->etmtrigger.ResourceB = 0x00;  // don't care
  // TraceEnable Control Register 1:
  ptm_cpu0->etmtecr1.TraceSSEn   = 0;     // unaffected by TraceEnable start/stop block
  ptm_cpu0->etmtecr1.ExcIncFlag  = 1;     // exclude
  ptm_cpu0->etmtecr1.AddrCompSel = 0x0;   // no address range
  // CoreSight Trace ID Register:
  ptm_cpu0->etmtraceidr.TraceID  = 0x42;  // non-zero, not in range 0x70-0x7F
  // Address Comparator registers:
  ptm_cpu0->etmacvr1.Address   = (uint32_t)( theStartAddress );
  ptm_cpu0->etmacvr2.Address   = (uint32_t)( theStopAddress  );
  //
  ptm_cpu0->etmcr.ProgBit = 0;
  while ( ptm_cpu0->etmsr.ProgBit != 0 ) {
    // wait
  }
  ptm_cpu0->lar = 0x00000000;

  /***********************/
  /** configure funnel: **/
  /***********************/
  funnel->lar                  = 0xC5ACCE55;
  funnel->control.EnableSlave0 = 1;
  funnel->control.MinHoldTime  = 0x3;
  funnel->lar                  = 0x00000000;

  /********************/
  /** configure ETB: **/
  /********************/
  etb->lar             = 0xC5ACCE55;
  etb->ctl.TraceCaptEn = 0;
  while ( etb->ffsr.FtStopped == 0 ) {
    // wait
  }
  // fill the buffer with 0x00:
  etb->rwp.value       = 0x000;
  for ( uint16_t i = 0x000 ; i < 0x400 ; i++ ) {
    etb->rwd.value = 0x00000000;
    // write pointer is automatically incremented
    __asm__ __volatile__ ("isb" : : : "memory");
    __asm__ __volatile__ ("dsb" : : : "memory");
  }
  etb->ctl.TraceCaptEn = 1;
  etb->lar             = 0x00000000;

  return;
}

/******************************************************************************/

/**
 * \brief
 *  Reads the content of the ETB and copies it at a given address.
 */
void copyEtbContent(
  uint32_t *theDestinationAddress,  //!< start address for the destination
  uint16_t theSize                  //!< number of 32-bit words to copy
) {

  etb->lar             = 0xC5ACCE55;
  etb->ctl.TraceCaptEn = 0;
  while ( etb->ffsr.FtStopped == 0 ) {
    // wait
  }
  etb->rrp.value       = 0x000;
  for ( uint16_t i = 0x000 ; i < theSize ; i++ ) {
    theDestinationAddress[i] = etb->rrd.value;
    // read pointer is automatically incremented
  }
  etb->rrp.value       = 0x000;
  etb->ctl.TraceCaptEn = 1;
  etb->lar             = 0x00000000;

  return;
}

/******************************************************************************/

/**
 * \brief
 *  Calls \ref copyEtbContent() with forced parameters so that we can call it
 *  from gdb without setting the parameters first.
 */
void debug_copyEtbContent(
  void
) {
  copyEtbContent((uint32_t *)(0x01001000), 4096);
  return;
}

/******************************************************************************/

/**
 * \brief
 *  Configures the GPIO that controls the reset of the logic analyser.
 */
void logicAnalyser_configure(
  void
) {
  gpio_setDirection(  GPIO_LOGIC_ANALYSER_RESET, GPIO_DIRECTION_OUTPUT);
  gpio_setOuputEnable(GPIO_LOGIC_ANALYSER_RESET, 1);
}

/******************************************************************************/

/**
 * \brief
 *  Sets the GPIO that controls the reset of the logic analyser, waits a bit and
 *  unsets the GPIO.
 */
void logicAnalyser_reset(
  void
) {
  uint32_t *theAddress;

  gpio_write(GPIO_LOGIC_ANALYSER_RESET, 1);

  /**
   * Fills the content of the BRAM with zeros.
   * Also we keep the reset high to make sure the clock of the FPGA issued at
   * least one rising edge.
   */
  theAddress = (uint32_t *)(&__logica_low);
  while ( theAddress < (uint32_t *)(&__logica_top) ) {
    *theAddress = 0x00000000;
    theAddress++;
  }

  gpio_write(GPIO_LOGIC_ANALYSER_RESET, 0);

  return;
}

/******************************************************************************/

/**
 * \brief
 *  Prints the content of the BRAM to stdout with the same format as gdb.
 */
void logicAnalyser_printf(
  void
) {
  uint32_t *theAddress;

  theAddress = (uint32_t *)(&__logica_low);
  while ( theAddress < (uint32_t *)(&__logica_top) ) {
    printf("%p:\t", theAddress);
    printf("0x%08lx\t", *theAddress++);
    printf("0x%08lx\t", *theAddress++);
    printf("0x%08lx\t", *theAddress++);
    printf("0x%08lx\n", *theAddress++);
  }

  return;
}

/******************************************************************************/

void* mmuSetAddress(
  intptr_t baseVirtAddr,
  intptr_t basePhysAddr,
  size_t size
) {
  uint32_t data;
  uint32_t addrSize;
  uint32_t *ttAddr;
  uint32_t tableIndex;
  uint32_t desc;
  uint32_t i;
  const uint32_t pageMask = MMU_PAGE_SIZE - 1;

  //__asm__ __volatile__("mrs %0, cpsr" : "=r"(data));              // CPSR
  __asm__ __volatile__("mrc p15, 0, %0, c2, c0, 2" : "=r"(data)); // TTBCR
  addrSize = data & 0x7;

  // Lecture du registre TTBR0
  __asm__ __volatile__("mrc p15, 0, %0, c2, c0, 0" : "=r"(data));
  ttAddr = (uint32_t *)(data & ~((1 << (14 - addrSize)) - 1));

  if ( !ttAddr ) {
    return (void *)basePhysAddr;
  }

  for ( i = 0; i < size; i += MMU_PAGE_SIZE ) {
    uint32_t sectionBaseAddr;
    tableIndex = ((baseVirtAddr + i) >> 20) & ((1 << (31-addrSize-20+1))-1);
    desc = ttAddr[tableIndex];

    sectionBaseAddr = (basePhysAddr + i) & ~pageMask;
    desc = (desc & pageMask) | sectionBaseAddr;
    ttAddr[tableIndex] = desc;

    __asm__ __volatile__("isb");
    __asm__ __volatile__("dsb");
  }

  return (void *)(baseVirtAddr & ~pageMask);
}

/******************************************************************************/

void attack_copyCode(
  uint32_t *theDestination,
  uint32_t *theSourceStart,
  uint32_t *theSourceStop
) {
  while ( theSourceStart < theSourceStop ) {
    *(theDestination++) = *(theSourceStart++);
  }
  return;
}

/**
 * \brief
 *  An attack that focuses on getting the same read signals when reading in the
 *  BRAM.
 *
 * \details
 *  Shows the differences between reading from BRAM using only one register and
 *  reading using 8 registers. The weakness of this attack is that reading using
 *  only one register does not match a fetch.
 */
void attack_readAndBranch1(
  void
) {
  __asm__ __volatile__("__attack_readAndBranch1_start:");

  /*
   * read 8 words and increment r0 by steps:
   */
  __asm__ __volatile__("mov r0, #0x40000000");
  __asm__ __volatile__("ldm r0!, {r1}");
  __asm__ __volatile__("ldm r0!, {r1}");
  __asm__ __volatile__("ldm r0!, {r1}");
  __asm__ __volatile__("ldm r0!, {r1}");
  __asm__ __volatile__("ldm r0!, {r1}");

  /*
   * indirect branch to force CoreSight to output a trace:
   */
  __asm__ __volatile__("add r9, pc, #0"); // pc = pc + 4; r3 = pc + 4 + 0;
  __asm__ __volatile__("mov pc, r9");

  __asm__ __volatile__("ldm r0!, {r1}");
  __asm__ __volatile__("ldm r0!, {r1}");
  __asm__ __volatile__("ldm r0!, {r1}");


  /*
   * read 8 words and increment r1:
   */
  __asm__ __volatile__("ldm r0 , {r1, r2, r3, r4, r5, r6, r7, r8}");
  __asm__ __volatile__("ldm r0!, {r1, r2, r3, r4, r5, r6, r7, r8}");

  /*
   * indirect branch to force CoreSight to output a trace:
   */
  __asm__ __volatile__("add r9, pc, #4"); // pc = pc + 4; r3 = pc + 4 + 4;
  __asm__ __volatile__("mov pc, r9");
  __asm__ __volatile__("nop");
  __asm__ __volatile__("nop");


  /*
   * read 8 words twice and increment r1:
   */
  __asm__ __volatile__("ldm r0 , {r1, r2, r3, r4, r5, r6, r7, r8}");
  __asm__ __volatile__("ldm r0!, {r1, r2, r3, r4, r5, r6, r7, r8}");

  /*
   * indirect branch to force CoreSight to output a trace:
   */
  __asm__ __volatile__("add r9, pc, #12"); // pc = pc + 4; r3 = pc + 4 + 12;
  __asm__ __volatile__("mov pc, r9");
  __asm__ __volatile__("nop");
  __asm__ __volatile__("nop");
  __asm__ __volatile__("nop");
  __asm__ __volatile__("nop");


  /*
   * read 8 words twice and increment r1:
   */
  __asm__ __volatile__("ldm r0 , {r1, r2, r3, r4, r5, r6, r7, r8}");
  __asm__ __volatile__("ldm r0!, {r1, r2, r3, r4, r5, r6, r7, r8}");

  __asm__ __volatile__("bx lr");
  __asm__ __volatile__("__attack_readAndBranch1_stop:");
  return;
}


/**
 * \brief
 *  An attack that focuses on the destination address for the indirect branch.
 *
 * \details
 *  Here we try to not rely on the number of registers.
 *  The weakness of this attack is that we "add r9, pc, #16" and "mov pc, r9"
 *  after the reads. This introduces a delay between the 1st and the 2nd read.
 *  Same goes between the 3rd and the 4th read, and betweeen 5th and the 6th
 *  read.
 */
void attack_readAndBranch2(
  void
) {
  __asm__ __volatile__("__attack_readAndBranch2_start:");

  /*
   * read 8 words and increment r0:
   */
  __asm__ __volatile__("mov r0, #0x40000000");
  __asm__ __volatile__("ldm r0!, {r1, r2, r3, r4, r5, r6, r7, r8}");

  /*
   * indirect branch to force CoreSight to output a trace:
   */
  __asm__ __volatile__("add r9, pc, #16"); // pc = pc + 4; r3 = pc + 4 + 16;
  __asm__ __volatile__("mov pc, r9");
  __asm__ __volatile__("nop");
  __asm__ __volatile__("nop");
  __asm__ __volatile__("nop");
  __asm__ __volatile__("nop");


  /*
   * read 8 words twice and increment r0:
   */
  __asm__ __volatile__("ldm r0 , {r1, r2, r3, r4, r5, r6, r7, r8}");
  __asm__ __volatile__("ldm r0!, {r1, r2, r3, r4, r5, r6, r7, r8}");

  /*
   * indirect branch to force CoreSight to output a trace:
   */
  __asm__ __volatile__("add r9, pc, #16"); // pc = pc + 4; r3 = pc + 4 + 16;
  __asm__ __volatile__("mov pc, r9");
  __asm__ __volatile__("nop");
  __asm__ __volatile__("nop");
  __asm__ __volatile__("nop");
  __asm__ __volatile__("nop");


  /*
   * read 8 words twice and increment r0:
   */
  __asm__ __volatile__("ldm r0 , {r1, r2, r3, r4, r5, r6, r7, r8}");
  __asm__ __volatile__("ldm r0!, {r1, r2, r3, r4, r5, r6, r7, r8}");

  /*
   * indirect branch to force CoreSight to output a trace:
   */
  __asm__ __volatile__("add r9, pc, #16"); // pc = pc + 4; r3 = pc + 4 + 16;
  __asm__ __volatile__("mov pc, r9");
  __asm__ __volatile__("nop");
  __asm__ __volatile__("nop");
  __asm__ __volatile__("nop");
  __asm__ __volatile__("nop");


  /*
   * read 8 words twice and increment r0:
   */
  __asm__ __volatile__("ldm r0 , {r1, r2, r3, r4, r5, r6, r7, r8}");
  __asm__ __volatile__("ldm r0!, {r1, r2, r3, r4, r5, r6, r7, r8}");

  __asm__ __volatile__("bx lr");
  __asm__ __volatile__("__attack_readAndBranch2_stop:");
  return;
}

/**
 * \brief
 *  An attack that focuses on getting coresight informations at the right time
 *  for the indirect branch.
 *
 * \details
 *  Here we try to have the indirect branches at the same time as the fetch.
 *  This attack gives the exact same traces on the read access in the BRAM.
 *  There is a tiny difference in the "trace_data" but this is because the
 *  addresses do not match. We must reconfigure the MMU to have the same address
 *  and the "trace_data" matches (see attack 4).
 *  The weakness of this attack is that we are limitted by the number of
 *  registers.
 */
void attack_readAndBranch3(
  void
) {
  __asm__ __volatile__("__attack_readAndBranch3_start:");

  /*
   * prepare for more than one indirect branch:
   */
  __asm__ __volatile__("add r9,  pc,  #0"); // pc = pc + 4; r3 = pc + 4 + 0;
  __asm__ __volatile__("add r9,  r9,  #24");
  __asm__ __volatile__("add r10, r9,  #32");
  __asm__ __volatile__("add r11, r10, #32");

  /*
   * read 8 words and increment r0:
   */
  __asm__ __volatile__("mov r0, #0x40000000");
  __asm__ __volatile__("ldm r0!, {r1, r2, r3, r4, r5, r6, r7, r8}");

  /*
   * indirect branch to force CoreSight to output a trace:
   */
  __asm__ __volatile__("mov pc, r9");
  __asm__ __volatile__("nop");


  /*
   * read 8 words twice and increment r0:
   */
  __asm__ __volatile__("ldm r0 , {r1, r2, r3, r4, r5, r6, r7, r8}");
  __asm__ __volatile__("ldm r0!, {r1, r2, r3, r4, r5, r6, r7, r8}");

  /*
   * indirect branch to force CoreSight to output a trace:
   */
  __asm__ __volatile__("mov pc, r10");
  __asm__ __volatile__("nop");
  __asm__ __volatile__("nop");
  __asm__ __volatile__("nop");
  __asm__ __volatile__("nop");
  __asm__ __volatile__("nop");


  /*
   * read 8 words twice and increment r0:
   */
  __asm__ __volatile__("ldm r0 , {r1, r2, r3, r4, r5, r6, r7, r8}");
  __asm__ __volatile__("ldm r0!, {r1, r2, r3, r4, r5, r6, r7, r8}");

  /*
   * indirect branch to force CoreSight to output a trace:
   */
  __asm__ __volatile__("mov pc, r11");
  __asm__ __volatile__("nop");
  __asm__ __volatile__("nop");
  __asm__ __volatile__("nop");
  __asm__ __volatile__("nop");
  __asm__ __volatile__("nop");


  /*
   * read 8 words twice and increment r0:
   */
  __asm__ __volatile__("ldm r0 , {r1, r2, r3, r4, r5, r6, r7, r8}");
  __asm__ __volatile__("ldm r0!, {r1, r2, r3, r4, r5, r6, r7, r8}");

  __asm__ __volatile__("bx lr");
  __asm__ __volatile__("__attack_readAndBranch3_stop:");
  return;
}

/******************************************************************************/

/**
 * \brief
 *  TODO.
 */
int main (
  void
) {
  unsigned int theSelect;
  uint32_t *theBramAddress   = (uint32_t *)(&__sw_att_low);
  uint32_t *theAttackAddress = (uint32_t *)(ATTACK_ADDRESS);
  const uint32_t theCoreSightOffset = (uint32_t)(&__sw_att_top - &__sw_att_low);


  Xil_DisableMMU();
  logicAnalyser_configure();

  /*
   * first, we read once in the BRAM so that the slave takes the same number of
   * clock cycles to read/fetch.
   */
  __asm__ __volatile__("mov r1, %0" :: "r"(&__sw_att_low));
  __asm__ __volatile__("ldm r1!, {r2, r3, r4, r5, r6, r7, r8, r9}");

  logicAnalyser_reset();


  // selection of the code to run, can be modified from the UART or by gdb:
  theSelect = 0x5d;
  __asm__ __volatile__("__breakpoint_select:");

  if ( theSelect == 0x5d ) {
    /* running from the SD card */
    printf("** Execution of an attack. Please enter the attack number:\n");
    printf("- 0 is reserved to the execution of the BRAM code\n");
    printf("- a higher number gives the index of the attack\n");
    theSelect = (unsigned int)(getchar() - '0');
    printf("You have selected: %u\n", theSelect);
  }


  if ( theSelect == 0 ) {
  /* execution of the code in the BRAM */
    configureCoresight(theBramAddress, theBramAddress + theCoreSightOffset);
    __asm__ __volatile__("mov r1, %0" :: "r"(theBramAddress));
    __asm__ __volatile__("mov lr, pc");
    __asm__ __volatile__("mov pc, r1");
  } else {
  /* execution of an attack */

    if ( theSelect == 1 ) {
      attack_copyCode( theAttackAddress,
                       (uint32_t *)(&__attack_readAndBranch1_start),
                       (uint32_t *)(&__attack_readAndBranch1_stop) );
      configureCoresight(theAttackAddress, theAttackAddress+theCoreSightOffset);
      __asm__ __volatile__("mov r1, %0" :: "r"(theAttackAddress));
      __asm__ __volatile__("mov lr, pc");
      __asm__ __volatile__("mov pc, r1");
    }
    else
    if ( theSelect == 2 ) {
      attack_copyCode( theAttackAddress,
                       (uint32_t *)(&__attack_readAndBranch2_start),
                       (uint32_t *)(&__attack_readAndBranch2_stop) );
      configureCoresight(theAttackAddress, theAttackAddress+theCoreSightOffset);
      __asm__ __volatile__("mov r1, %0" :: "r"(theAttackAddress));
      __asm__ __volatile__("mov lr, pc");
      __asm__ __volatile__("mov pc, r1");
    }
    else
    if ( theSelect == 3 ) {
      attack_copyCode( theAttackAddress,
                       (uint32_t *)(&__attack_readAndBranch3_start),
                       (uint32_t *)(&__attack_readAndBranch3_stop) );
      configureCoresight(theAttackAddress, theAttackAddress+theCoreSightOffset);
      __asm__ __volatile__("mov r1, %0" :: "r"(theAttackAddress));
      __asm__ __volatile__("mov lr, pc");
      __asm__ __volatile__("mov pc, r1");
    }
    else
    if ( theSelect == 4 ) {
      // MMU, switch the location of the BRAM with the location of the attack:
      Xil_DisableMMU();
      mmuSetAddress( (uint32_t)(theBramAddress),   (uint32_t)(theAttackAddress),
                     MMU_PAGE_SIZE );
      mmuSetAddress( (uint32_t)(theAttackAddress), (uint32_t)(theBramAddress),
                     MMU_PAGE_SIZE );
      Xil_EnableMMU();
      //
      theBramAddress   = (uint32_t *)(ATTACK_ADDRESS);
      theAttackAddress = (uint32_t *)(&__sw_att_low);

      /* execution of the code in the BRAM */
      configureCoresight(theBramAddress, theBramAddress + theCoreSightOffset);
      __asm__ __volatile__("nop");
      __asm__ __volatile__("mov r1, %0" :: "r"(theBramAddress));
      __asm__ __volatile__("mov lr, pc");
      __asm__ __volatile__("mov pc, r1");
    }
    else {
      printf("Wrong value for selection.\n");
      while (1) {
        // do nothing
      }
    }

  }

  logicAnalyser_printf();

  while (1) {
    // infinite loop
    __asm__ __volatile__("__my_breakpoint:");
  }

  return 0;
}

