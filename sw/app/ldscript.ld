
/*
  Copyright (C) 2021 Jonathan Certes

  This program is free software: you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free Software
  Foundation, either version 3 of the License, or (at your option) any later
  version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  this program. If not, see <http://www.gnu.org/licenses/>.
 */

/* dedicated memories in AXI-Lite slaves: must match the hardware */
SW_ATT_ORIGIN = 0x40000000;
SW_ATT_LENGTH = 0x4000;
/**/
LOGICA_ORIGIN = 0x42000000;
LOGICA_LENGTH = 0x1000;

/* dedicated memories in the DDR: */
STACK_LENGTH       = 0x2000;
HEAP_LENGTH        = 0x2000;
CODE_LOADER_LENGTH = 0x20000;
UNDEF_STACK_LENGTH = 0x400;   /* used by xilinx functions that flush the cache */


/* Define Memories in the system */
MEMORY
{
  rom_sw_att (rx) : ORIGIN = SW_ATT_ORIGIN LENGTH = SW_ATT_LENGTH
  rom_logica (rw) : ORIGIN = LOGICA_ORIGIN LENGTH = LOGICA_LENGTH

  ps7_ddr_0         : ORIGIN = 0x100000,   LENGTH = 0x1FF00000
  ps7_qspi_linear_0 : ORIGIN = 0xFC000000, LENGTH = 0x1000000
  ps7_ram_0         : ORIGIN = 0x0,        LENGTH = 0x30000
  ps7_ram_1         : ORIGIN = 0xFFFF0000, LENGTH = 0xFE00
}


/* Specify the default entry point to the program */
ENTRY(_start)


/* Define the sections, and where they are mapped in memory */
SECTIONS
{

  .startup : {
    . = ALIGN(4);
    *startup.o(.text)
  } > ps7_ddr_0

  /* critical region for the software */
  .sw_att : {
    PROVIDE(__sw_att_low = .);
    . = . + SW_ATT_LENGTH;
    PROVIDE(__sw_att_top = .);
  } > rom_sw_att

  /* logic analyser */
  .logica : {
    PROVIDE(__logica_low = .);
    . = . + LOGICA_LENGTH;
    PROVIDE(__logica_top = .);
  } > rom_logica

  /*****************************************************************************
  /* Rest of the code, bare metal application:
   */
  .text : {
    /* rest of the code */
    . = ALIGN(4);
    *(.text)
    *(.data)
    *(.rodata)
    *(.rodata.*)
    *(.bss COMMON)
  } > ps7_ddr_0

  .ARM : {
     PROVIDE(__exidx_start = .);
    . = ALIGN(4);
     *(.ARM.exidx*)
     *(.gnu.linkonce.armexidix.*.*)
     PROVIDE(__exidx_end = .);
  } > ps7_ddr_0


  /*****************************************************************************
   * heap and stack
   */
  .heap (NOLOAD) : {
    PROVIDE(__heap_low  = .);
    . = . + HEAP_LENGTH;
    PROVIDE(__heap_top  = .);
  } > ps7_ddr_0

  .stack (NOLOAD) : {
    PROVIDE(__stack_low = .);
    PROVIDE(_stack_end  = .);  /* used by xilinx, identical to __stack_low */
    . = . + STACK_LENGTH;
    PROVIDE(__stack_top = .);
  } > ps7_ddr_0


  /*****************************************************************************
   * undef stack is used by xilinx functions that flush the cache
   */
  .undef_stack (NOLOAD) : {
    PROVIDE(_undef_stack_end = .);
    . = . + UNDEF_STACK_LENGTH;
    . = ALIGN(16);
    PROVIDE(__undef_stack    = .);
  } > ps7_ddr_0

}

