
#
ifndef LD
	echo "You must specify a linker script in variable LD."
	exit 1
endif
#
ifndef SRC
	echo "You must specify the source to compile in variable SRC."
	exit 1
endif
#
ifndef BIN
	echo "You must specify the binary name in variable BIN."
	exit 1
endif

#===============================================================================

INCLUDE = $(addprefix -I , $(sort $(dir ${SRC})))
CCOUT   = $(addsuffix .o, $(basename ${SRC}))

ARMGNU = arm-none-eabi-
ARCH   = -mcpu=cortex-a9
ASOPS  = -g
COPS   = -g -no-pie -Wall -O0 -nostdlib
LOPS   = -L "/usr/lib/arm-none-eabi/newlib/" -lc \
         -L "/usr/lib/gcc/arm-none-eabi/8.3.1/" -lgcc -lnosys

#===============================================================================

all : ${BIN}.elf


${BIN}.elf : ${LD} ${CCOUT}
	$(ARMGNU)ld -T ${LD} ${CCOUT} -o $@ ${LOPS}


%.o : %.c
	$(ARMGNU)gcc ${ARCH} ${INCLUDE} -c ${COPS} $^ -o $@

%.o : %.s
	$(ARMGNU)as  ${ARCH} ${ASOPS} $^ -o $@


clean :
	$(RM) ${CCOUT}
	$(RM) ${BIN}.elf

