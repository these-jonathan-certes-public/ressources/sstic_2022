
/*
  Copyright (C) 2021 Jonathan Certes

  This program is free software: you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free Software
  Foundation, either version 3 of the License, or (at your option) any later
  version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <fcntl.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>

#define BUFSIZE 32

int main(
  int   argc,
  char* argv[]
) {
  int theFileDescriptor, theCode;
  unsigned char tampon[BUFSIZE];
  unsigned int  theIndex;
  ssize_t theReadSize;

  if ( argc < 2 ) {
    printf("** Error ** you must specify at least one argument.\n");
    return 1;
  }

  theFileDescriptor = open(argv[1], O_RDONLY);
  if ( theFileDescriptor < 0 ) {
    perror("** Error ** ");
    return 1;
  }

  theIndex = 0;
  do {
    memset(tampon, '\0', BUFSIZE);
    theReadSize = read(theFileDescriptor, tampon, BUFSIZE);
    if ( theReadSize < 0 ) {
      perror("** Error ** ");
    }

    printf(".INIT_%02i(256'h", theIndex);
    for ( unsigned int i = 1; i <= BUFSIZE; i++ ) {
      printf("%02x", tampon[BUFSIZE-i]);
    }
    printf("),\n");
    theIndex++;
  } while ( theReadSize == BUFSIZE );

  theCode = close(theFileDescriptor);
  if ( theCode != 0 ) {
    perror("** Error ** ");
  }

  return 0;
}

