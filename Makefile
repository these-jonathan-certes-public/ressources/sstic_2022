
OCD = ../openocd_zynq.tcl

#===============================================================================

all: openocd


openocd: ${OCD} hw/design_0_wrapper.bit exec_from_bram.gdb
	make -C sw
	make -C sw/sw_att/
	# TO BE RUN IN AN OTHER TERMINAL:
	# gdb-multiarch -ex "set architecture armv7" -ex "target extended-remote localhost:3333" --command="gdbinit.gdb"
	#
	/usr/bin/openocd -f $<


qemu: exec_from_bram.gdb
	make -C sw
	# TO BE RUN IN AN OTHER TERMINAL:
	# gdb-multiarch -ex "target remote localhost:1234" --command="$^"
	#
	qemu-system-arm -machine xilinx-zynq-a9 -cpu cortex-a9 -m 1132M -nographic \
	  -serial null -serial mon:stdio -gdb tcp::1234


sdcard: sdcard/boot.bif sw/fsbl/main.elf sw/app/main.elf hw/design_0_wrapper.bit
	make -C sw
	make -C sdcard
	# copy file sdcard/boot.bin on the SD card and boot the Zybo ; connect to the
	# board with:
	# pyserial-miniterm /dev/ttyUSB1 115200


hw/design_0_wrapper.bit:
	make -C hw

#===============================================================================

clean:
	make -C sw         clean
	make -C hw         clean
	make -C sdcard     clean
	make -C sw/sw_att/ clean
	make -C hw/logic_analyzer/decode clean
