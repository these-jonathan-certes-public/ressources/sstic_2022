/*
 * Copyright (C) 2021 Jonathan Certes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <systemc.h>
#include <verilated_vcd_sc.h>

#include "Vdecode.h"

/**
 * Number of uint32_t in the trace.
 */
#define DATA_COUNT 1024

uint32_t theDataValue[DATA_COUNT] = {
  /* START */
  /* STOP */
};

/******************************************************************************/

/**
 * \brief
 *  Generates signals for the testbench.
 */
SC_MODULE( stimulus ) {

  sc_in  <bool>     clk;
  sc_out <bool>     nrst;
  sc_out <uint32_t> data;

  int32_t count = 0;

  /**
   * \brief
   *  Generates stimulus.
   */
  void stimulusGen( void ) {

    if ( count < 0 ) {
      nrst.write(false);
      data.write(0x00000000);
    } else {
      nrst.write(true);
      data.write( theDataValue[count]);
    }

    count = count + 1;
    if ( count >= (int32_t)(sizeof(theDataValue)/sizeof(uint32_t)) ) {
      printf("count = %i\n", count);
      sc_stop();
    }
  }

  /**
   * \brief
   *  Active on rising edge of clk.
   */
  SC_CTOR(stimulus) {
    SC_METHOD(stimulusGen);
    sensitive << clk.pos();
  }

};

/******************************************************************************/

/**
 * \brief
 *  Generates the clock for the testbench.
 */
SC_MODULE( clkgen ) {

  sc_out <bool> clk;

  /**
   * \brief
   *  Custom function for the clock.
   */
  void doGenerate( void ) {
    clk.write(false);
    wait(10, SC_NS);
    while (1) {
      clk.write( !clk.read() );
      wait(5, SC_NS);
    }
  }

  /**
   * \brief
   *  Uses SC_THREAD so that wait() function can be called.
   */
  SC_CTOR(clkgen) {
    SC_THREAD(doGenerate);
  }

};

/******************************************************************************/

int sc_main(
  int argc,
  char* argv[]
) {
  sc_signal <bool>     nrst;
  sc_signal <bool>     clk;
  sc_signal <uint32_t> data;

  int theCode;
  // to read the file that is output by gdb (max 256 char by line):
  FILE *theFileDescriptor;
  const unsigned MAX_LENGTH = 256;
  char theLine[MAX_LENGTH];
  char *theReturn;
  // for scanf:
  uint32_t theAddress, theValue0, theValue1, theValue2, theValue3;
  unsigned int theMatchCount;
  // to keep count:
  unsigned int theDataIndex;

  if ( argc > 1 ) {
    /* parse gdb output file */
    theFileDescriptor = fopen(argv[1], "r");
    if ( theFileDescriptor < 0 ) {
      perror("** Error ** ");
      return 1;
    }

    theDataIndex = 0;
    do {
      theReturn = fgets(theLine, MAX_LENGTH, theFileDescriptor);
      theMatchCount = sscanf(theLine, "%x:\t%x\t%x\t%x\t%x",
              &theAddress, &theValue0, &theValue1, &theValue2, &theValue3);
      if ( theMatchCount > 1 ) { theDataValue[theDataIndex++] = theValue0; }
      if ( theMatchCount > 2 ) { theDataValue[theDataIndex++] = theValue1; }
      if ( theMatchCount > 3 ) { theDataValue[theDataIndex++] = theValue2; }
      if ( theMatchCount > 4 ) { theDataValue[theDataIndex++] = theValue3; }
    } while ( (theReturn != NULL)   &&
              (strlen(theLine) > 0) &&
              (theDataIndex < DATA_COUNT)
            );

    theCode = fclose(theFileDescriptor);
    if ( theCode != 0 ) {
      perror("** Error ** ");
    }
  }

  stimulus i_stimulus("data");
           i_stimulus.nrst(  nrst  );
           i_stimulus.clk (  clk   );
           i_stimulus.data(  data  );

  clkgen i_clkgen("i_clkgen");
         i_clkgen.clk( clk );

  Vdecode i_decode("i_decode");
          i_decode.nrst(  nrst  );
          i_decode.clk (  clk   );
          i_decode.data(  data  );

  // creation of a VCD file:
  VerilatedVcdSc* theOutput = new VerilatedVcdSc;
  Verilated::traceEverOn(true);
  i_decode.trace(theOutput, 99); // depth in the hierarchy
  theOutput->open("output.vcd");

  sc_start();
  theOutput->close();
  return 0;
}
