/*
 * Copyright (C) 2021 Jonathan Certes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

/* verilator lint_off UNUSED */

module decode
(
  input        nrst,
  input        clk,
  input [31:0] data
);

  wire        sw_arvalid;
  wire [13:0] sw_araddr;
  wire        sw_arready;
  //
  wire  [7:0] trace_data;
  wire        trace_ctl;

  assign {
    sw_arvalid,
    sw_araddr,
    sw_arready
  } = data[31:16];

  assign {
    trace_data,
    trace_ctl
  } = data[8:0];

/******************************************************************************/
/* DEBUG: adding CoreSight packet decompresser and decoder to the testbench so
 * that we can observe the destination addresses.
 */

  // trick the FSM so that it acts as it already received an A-sync packet:
  initial begin
    i_coresight2pl.i_async_decompress.state =
    i_coresight2pl.i_async_decompress.state_header;
    //
    i_coresight2pl.i_async_decompress.count = 4;
  end

  /* verilator lint_off PINMISSING */
  coresight2pl i_coresight2pl (
    .nrst(       nrst       ),
    .clk(        clk        ),
    .trace_data( trace_data ),
    .trace_ctl(  trace_ctl  )
  );
  /* verilator lint_on  PINMISSING */

endmodule

/* verilator lint_on UNUSED */
