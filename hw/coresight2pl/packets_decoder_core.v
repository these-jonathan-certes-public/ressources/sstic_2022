/*
 * Copyright (C) 2021 Jonathan Certes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

/* verilator lint_off UNUSED */
/* verilator lint_off BLKSEQ */

/**
 * \brief
 *  Decodes data from Coresight packets of raw data.
 *
 * \details
 *  After a reset, all outputs are set to zero.
 *
 *
 *  # Decodes branch adress packets when <tt> branch_ready </tt> is asserted:
 *
 *  Output <tt> decoded_isetstate </tt> does not change if fifth address byte is
 *  not present. Otherwise, it indicates the instruction set state:
 *  - 2'b00 if ARM
 *  - 2'b01 if Thumb
 *  - 2'b10 if Jazelle
 *
 *  Address is decoded according to ARM Coresight PFT architecture
 *  specification, function of <tt> decoded_isetstate </tt> on the next state.
 *
 *  Exception, if absent from the packet, is set to zero. If only the first
 *  exception byte is present, the missing bits are set to zero. Otherwise, if
 *  present, exception is decoded according to ARM Coresight PFT architecture
 *  specification.
 *
 *  Cycle count is decoded according to ARM Coresight PFT architecture
 *  specification.
 *
 *
 *  # Decodes isync packets when <tt> isync_ready </tt> is asserted:
 *
 *  Output <tt> decoded_isetstate </tt> indicates the instruction set state
 *  function of T:
 *  - 2'b00 if ARM
 *  - 2'b01 if Thumb
 *
 *  Address, information byte, cycle count and context ID are decoded according
 *  to ARM Coresight PFT architecture specification.
 *
 */
module packets_decoder_core
#(
  parameter p_none      = 4'b0000,
  parameter p_isync     = 4'b0001,
  parameter p_atom      = 4'b0010,
  parameter p_branch    = 4'b0011,
  parameter p_waypoint  = 4'b0100,
  parameter p_trigger   = 4'b0101,
  parameter p_contextid = 4'b0110,
  parameter p_vmid      = 4'b0111,
  parameter p_timestamp = 4'b1000,
  parameter p_except    = 4'b1001,
  parameter p_ignore    = 4'b1010,
  //
  parameter is_arm     = 2'b00,
  parameter is_thumb   = 2'b01,
  parameter is_jazelle = 2'b10,
  parameter is_unknown = 2'b11
) (
  input nrst, //!< low level active reset
  input clk,  //!< clock

  input [3:0] available_packet,   //!< indicates which packet is currently decompressing
  input       packet_ready,  //!< indicates that the packet is decompressed
  input [7:0] data__0,  //!< packet byte index  0
  input [7:0] data__1,  //!< packet byte index  1
  input [7:0] data__2,  //!< packet byte index  2
  input [7:0] data__3,  //!< packet byte index  3
  input [7:0] data__4,  //!< packet byte index  4
  input [7:0] data__5,  //!< packet byte index  5
  input [7:0] data__6,  //!< packet byte index  6
  input [7:0] data__7,  //!< packet byte index  7
  input [7:0] data__8,  //!< packet byte index  8
  input [7:0] data__9,  //!< packet byte index  9
  input [7:0] data_10,  //!< packet byte index 10
  input [7:0] data_11,  //!< packet byte index 11
  input [7:0] data_12,  //!< packet byte index 12
  input [7:0] data_13,  //!< packet byte index 13
  input [7:0] data_14,  //!< packet byte index 14

  output  [1:0] decoded_isetstate,
  // separation to ease conception and model-checking:
  output decoded_address__0,  output decoded_address_16,
  output decoded_address__1,  output decoded_address_17,
  output decoded_address__2,  output decoded_address_18,
  output decoded_address__3,  output decoded_address_19,
  output decoded_address__4,  output decoded_address_20,
  output decoded_address__5,  output decoded_address_21,
  output decoded_address__6,  output decoded_address_22,
  output decoded_address__7,  output decoded_address_23,
  output decoded_address__8,  output decoded_address_24,
  output decoded_address__9,  output decoded_address_25,
  output decoded_address_10,  output decoded_address_26,
  output decoded_address_11,  output decoded_address_27,
  output decoded_address_12,  output decoded_address_28,
  output decoded_address_13,  output decoded_address_29,
  output decoded_address_14,  output decoded_address_30,
  output decoded_address_15,  output decoded_address_31,
  //
  output        hyp,
  output        altis,
  output        ns,
  output  [1:0] isync_reason,
  // separation to ease conception and model-checking:
  output cyclecount__0,   output cyclecount_16,
  output cyclecount__1,   output cyclecount_17,
  output cyclecount__2,   output cyclecount_18,
  output cyclecount__3,   output cyclecount_19,
  output cyclecount__4,   output cyclecount_20,
  output cyclecount__5,   output cyclecount_21,
  output cyclecount__6,   output cyclecount_22,
  output cyclecount__7,   output cyclecount_23,
  output cyclecount__8,   output cyclecount_24,
  output cyclecount__9,   output cyclecount_25,
  output cyclecount_10,   output cyclecount_26,
  output cyclecount_11,   output cyclecount_27,
  output cyclecount_12,   output cyclecount_28,
  output cyclecount_13,   output cyclecount_29,
  output cyclecount_14,   output cyclecount_30,
  output cyclecount_15,   output cyclecount_31,
  // separation to ease conception and model-checking:
  output  [7:0] contextid_0,
  output  [7:0] contextid_1,
  output  [7:0] contextid_2,
  output  [7:0] contextid_3,
  //
  output        atom_f,
  output  [8:0] decoded_exception
);

  /****************************************************************************/

  /**
   * \brief
   *  Updates 32-bits register <tt> r_cyclecount_* </tt> from 5 bytes of data.
   */
  task updateCyclecount(
    input [7:0] t_data_0, //!< byte 0 of cycle count section
    input [7:0] t_data_1, //!< byte 1 of cycle count section
    input [7:0] t_data_2, //!< byte 2 of cycle count section
    input [7:0] t_data_3, //!< byte 3 of cycle count section
    input [7:0] t_data_4  //!< byte 4 of cycle count section
  );
  begin
    r_cyclecount__0 <= t_data_0[2];   r_cyclecount_16 <= 1'b0;
    r_cyclecount__1 <= t_data_0[3];   r_cyclecount_17 <= 1'b0;
    r_cyclecount__2 <= t_data_0[4];   r_cyclecount_18 <= 1'b0;
    r_cyclecount__3 <= t_data_0[5];   r_cyclecount_19 <= 1'b0;
    r_cyclecount__4 <= 1'b0;          r_cyclecount_20 <= 1'b0;
    r_cyclecount__5 <= 1'b0;          r_cyclecount_21 <= 1'b0;
    r_cyclecount__6 <= 1'b0;          r_cyclecount_22 <= 1'b0;
    r_cyclecount__7 <= 1'b0;          r_cyclecount_23 <= 1'b0;
    r_cyclecount__8 <= 1'b0;          r_cyclecount_24 <= 1'b0;
    r_cyclecount__9 <= 1'b0;          r_cyclecount_25 <= 1'b0;
    r_cyclecount_10 <= 1'b0;          r_cyclecount_26 <= 1'b0;
    r_cyclecount_11 <= 1'b0;          r_cyclecount_27 <= 1'b0;
    r_cyclecount_12 <= 1'b0;          r_cyclecount_28 <= 1'b0;
    r_cyclecount_13 <= 1'b0;          r_cyclecount_29 <= 1'b0;
    r_cyclecount_14 <= 1'b0;          r_cyclecount_30 <= 1'b0;
    r_cyclecount_15 <= 1'b0;          r_cyclecount_31 <= 1'b0;
    //
    if ( t_data_0[6] ) begin
      r_cyclecount__4 <= t_data_1[0];
      r_cyclecount__5 <= t_data_1[1];
      r_cyclecount__6 <= t_data_1[2];
      r_cyclecount__7 <= t_data_1[3];
      r_cyclecount__8 <= t_data_1[4];
      r_cyclecount__9 <= t_data_1[5];
      r_cyclecount_10 <= t_data_1[6];
      if ( t_data_1[7] ) begin
        r_cyclecount_11 <= t_data_2[0];
        r_cyclecount_12 <= t_data_2[1];
        r_cyclecount_13 <= t_data_2[2];
        r_cyclecount_14 <= t_data_2[3];
        r_cyclecount_15 <= t_data_2[4];
        r_cyclecount_16 <= t_data_2[5];
        r_cyclecount_17 <= t_data_2[6];
        if ( t_data_2[7] ) begin
          r_cyclecount_18 <= t_data_3[0];
          r_cyclecount_19 <= t_data_3[1];
          r_cyclecount_20 <= t_data_3[2];
          r_cyclecount_21 <= t_data_3[3];
          r_cyclecount_22 <= t_data_3[4];
          r_cyclecount_23 <= t_data_3[5];
          r_cyclecount_24 <= t_data_3[6];
          if ( t_data_3[7] ) begin
            r_cyclecount_25 <= t_data_4[0];
            r_cyclecount_26 <= t_data_4[1];
            r_cyclecount_27 <= t_data_4[2];
            r_cyclecount_28 <= t_data_4[3];
            r_cyclecount_29 <= t_data_4[4];
            r_cyclecount_30 <= t_data_4[5];
            r_cyclecount_31 <= t_data_4[6];
          end
        end
      end
    end
  end
  endtask

  /****************************************************************************/

  /**
   * \brief
   *  Updates 32-bits register <tt> r_contextid_* </tt> from 4 bytes of data.
   */
  task updateContextid(
    input [7:0] t_data_0, //!< byte 0 of contextid section
    input [7:0] t_data_1, //!< byte 1 of contextid section
    input [7:0] t_data_2, //!< byte 2 of contextid section
    input [7:0] t_data_3  //!< byte 3 of contextid section
  );
  begin
    r_contextid_0 <= t_data_0;
    r_contextid_1 <= t_data_1;
    r_contextid_2 <= t_data_2;
    r_contextid_3 <= t_data_3;
  end
  endtask

  /****************************************************************************/

  reg [1:0] r_decoded_isetstate;  // WARNING: blocking assignment

  reg r_decoded_address__0; reg r_decoded_address_16;
  reg r_decoded_address__1; reg r_decoded_address_17;
  reg r_decoded_address__2; reg r_decoded_address_18;
  reg r_decoded_address__3; reg r_decoded_address_19;
  reg r_decoded_address__4; reg r_decoded_address_20;
  reg r_decoded_address__5; reg r_decoded_address_21;
  reg r_decoded_address__6; reg r_decoded_address_22;
  reg r_decoded_address__7; reg r_decoded_address_23;
  reg r_decoded_address__8; reg r_decoded_address_24;
  reg r_decoded_address__9; reg r_decoded_address_25;
  reg r_decoded_address_10; reg r_decoded_address_26;
  reg r_decoded_address_11; reg r_decoded_address_27;
  reg r_decoded_address_12; reg r_decoded_address_28;
  reg r_decoded_address_13; reg r_decoded_address_29;
  reg r_decoded_address_14; reg r_decoded_address_30;
  reg r_decoded_address_15; reg r_decoded_address_31;
  //
  reg        r_hyp;
  reg        r_altis;
  reg        r_ns;
  reg  [1:0] r_isync_reason;
  //
  reg r_cyclecount__0; reg r_cyclecount_16;
  reg r_cyclecount__1; reg r_cyclecount_17;
  reg r_cyclecount__2; reg r_cyclecount_18;
  reg r_cyclecount__3; reg r_cyclecount_19;
  reg r_cyclecount__4; reg r_cyclecount_20;
  reg r_cyclecount__5; reg r_cyclecount_21;
  reg r_cyclecount__6; reg r_cyclecount_22;
  reg r_cyclecount__7; reg r_cyclecount_23;
  reg r_cyclecount__8; reg r_cyclecount_24;
  reg r_cyclecount__9; reg r_cyclecount_25;
  reg r_cyclecount_10; reg r_cyclecount_26;
  reg r_cyclecount_11; reg r_cyclecount_27;
  reg r_cyclecount_12; reg r_cyclecount_28;
  reg r_cyclecount_13; reg r_cyclecount_29;
  reg r_cyclecount_14; reg r_cyclecount_30;
  reg r_cyclecount_15; reg r_cyclecount_31;
  //
  reg [7:0] r_contextid_0;
  reg [7:0] r_contextid_1;
  reg [7:0] r_contextid_2;
  reg [7:0] r_contextid_3;
  //
  reg       r_atom_f;
  reg [8:0] r_decoded_exception;

  initial begin
    r_decoded_isetstate  = is_unknown;
    //
    r_decoded_address__0 = 1'b0;  r_decoded_address_16 = 1'b0;
    r_decoded_address__1 = 1'b0;  r_decoded_address_17 = 1'b0;
    r_decoded_address__2 = 1'b0;  r_decoded_address_18 = 1'b0;
    r_decoded_address__3 = 1'b0;  r_decoded_address_19 = 1'b0;
    r_decoded_address__4 = 1'b0;  r_decoded_address_20 = 1'b0;
    r_decoded_address__5 = 1'b0;  r_decoded_address_21 = 1'b0;
    r_decoded_address__6 = 1'b0;  r_decoded_address_22 = 1'b0;
    r_decoded_address__7 = 1'b0;  r_decoded_address_23 = 1'b0;
    r_decoded_address__8 = 1'b0;  r_decoded_address_24 = 1'b0;
    r_decoded_address__9 = 1'b0;  r_decoded_address_25 = 1'b0;
    r_decoded_address_10 = 1'b0;  r_decoded_address_26 = 1'b0;
    r_decoded_address_11 = 1'b0;  r_decoded_address_27 = 1'b0;
    r_decoded_address_12 = 1'b0;  r_decoded_address_28 = 1'b0;
    r_decoded_address_13 = 1'b0;  r_decoded_address_29 = 1'b0;
    r_decoded_address_14 = 1'b0;  r_decoded_address_30 = 1'b0;
    r_decoded_address_15 = 1'b0;  r_decoded_address_31 = 1'b0;
    //
    r_hyp          = 1'b0;
    r_altis        = 1'b0;
    r_ns           = 1'b0;
    r_isync_reason = 2'b00;
    //
    r_cyclecount__0 = 1'b0;  r_cyclecount_16 = 1'b0;
    r_cyclecount__1 = 1'b0;  r_cyclecount_17 = 1'b0;
    r_cyclecount__2 = 1'b0;  r_cyclecount_18 = 1'b0;
    r_cyclecount__3 = 1'b0;  r_cyclecount_19 = 1'b0;
    r_cyclecount__4 = 1'b0;  r_cyclecount_20 = 1'b0;
    r_cyclecount__5 = 1'b0;  r_cyclecount_21 = 1'b0;
    r_cyclecount__6 = 1'b0;  r_cyclecount_22 = 1'b0;
    r_cyclecount__7 = 1'b0;  r_cyclecount_23 = 1'b0;
    r_cyclecount__8 = 1'b0;  r_cyclecount_24 = 1'b0;
    r_cyclecount__9 = 1'b0;  r_cyclecount_25 = 1'b0;
    r_cyclecount_10 = 1'b0;  r_cyclecount_26 = 1'b0;
    r_cyclecount_11 = 1'b0;  r_cyclecount_27 = 1'b0;
    r_cyclecount_12 = 1'b0;  r_cyclecount_28 = 1'b0;
    r_cyclecount_13 = 1'b0;  r_cyclecount_29 = 1'b0;
    r_cyclecount_14 = 1'b0;  r_cyclecount_30 = 1'b0;
    r_cyclecount_15 = 1'b0;  r_cyclecount_31 = 1'b0;
    //
    r_contextid_0 = 8'h00;
    r_contextid_1 = 8'h00;
    r_contextid_2 = 8'h00;
    r_contextid_3 = 8'h00;
    //
    r_atom_f            = 1'b0;
    r_decoded_exception = 9'b000000000;
  end

  always @( posedge(clk) ) begin
    if ( nrst == 1'b0 ) begin
      r_decoded_isetstate   = is_unknown;
      //
      r_decoded_address__0 <= 1'b0;  r_decoded_address_16 <= 1'b0;
      r_decoded_address__1 <= 1'b0;  r_decoded_address_17 <= 1'b0;
      r_decoded_address__2 <= 1'b0;  r_decoded_address_18 <= 1'b0;
      r_decoded_address__3 <= 1'b0;  r_decoded_address_19 <= 1'b0;
      r_decoded_address__4 <= 1'b0;  r_decoded_address_20 <= 1'b0;
      r_decoded_address__5 <= 1'b0;  r_decoded_address_21 <= 1'b0;
      r_decoded_address__6 <= 1'b0;  r_decoded_address_22 <= 1'b0;
      r_decoded_address__7 <= 1'b0;  r_decoded_address_23 <= 1'b0;
      r_decoded_address__8 <= 1'b0;  r_decoded_address_24 <= 1'b0;
      r_decoded_address__9 <= 1'b0;  r_decoded_address_25 <= 1'b0;
      r_decoded_address_10 <= 1'b0;  r_decoded_address_26 <= 1'b0;
      r_decoded_address_11 <= 1'b0;  r_decoded_address_27 <= 1'b0;
      r_decoded_address_12 <= 1'b0;  r_decoded_address_28 <= 1'b0;
      r_decoded_address_13 <= 1'b0;  r_decoded_address_29 <= 1'b0;
      r_decoded_address_14 <= 1'b0;  r_decoded_address_30 <= 1'b0;
      r_decoded_address_15 <= 1'b0;  r_decoded_address_31 <= 1'b0;
      //
      r_hyp          <= 1'b0;
      r_altis        <= 1'b0;
      r_ns           <= 1'b0;
      r_isync_reason <= 2'b00;
      //
      r_cyclecount__0 <= 1'b0;  r_cyclecount_16 <= 1'b0;
      r_cyclecount__1 <= 1'b0;  r_cyclecount_17 <= 1'b0;
      r_cyclecount__2 <= 1'b0;  r_cyclecount_18 <= 1'b0;
      r_cyclecount__3 <= 1'b0;  r_cyclecount_19 <= 1'b0;
      r_cyclecount__4 <= 1'b0;  r_cyclecount_20 <= 1'b0;
      r_cyclecount__5 <= 1'b0;  r_cyclecount_21 <= 1'b0;
      r_cyclecount__6 <= 1'b0;  r_cyclecount_22 <= 1'b0;
      r_cyclecount__7 <= 1'b0;  r_cyclecount_23 <= 1'b0;
      r_cyclecount__8 <= 1'b0;  r_cyclecount_24 <= 1'b0;
      r_cyclecount__9 <= 1'b0;  r_cyclecount_25 <= 1'b0;
      r_cyclecount_10 <= 1'b0;  r_cyclecount_26 <= 1'b0;
      r_cyclecount_11 <= 1'b0;  r_cyclecount_27 <= 1'b0;
      r_cyclecount_12 <= 1'b0;  r_cyclecount_28 <= 1'b0;
      r_cyclecount_13 <= 1'b0;  r_cyclecount_29 <= 1'b0;
      r_cyclecount_14 <= 1'b0;  r_cyclecount_30 <= 1'b0;
      r_cyclecount_15 <= 1'b0;  r_cyclecount_31 <= 1'b0;
      //
      r_contextid_0 <= 8'h00;
      r_contextid_1 <= 8'h00;
      r_contextid_2 <= 8'h00;
      r_contextid_3 <= 8'h00;
      //
      r_atom_f            <= 1'b0;
      r_decoded_exception <= 9'b000000000;
    end
    else if ( packet_ready && (available_packet == p_isync) ) begin

      /***************************
       ** decode isync packets: **
       ***************************/

      // address, 4 bytes:
      r_decoded_isetstate   = (data__1[0]) ? is_thumb : is_arm;
      //
      r_decoded_address__0 <= 1'b0;
      r_decoded_address__1 <= data__1[1];
      r_decoded_address__2 <= data__1[2];
      r_decoded_address__3 <= data__1[3];
      r_decoded_address__4 <= data__1[4];
      r_decoded_address__5 <= data__1[5];
      r_decoded_address__6 <= data__1[6];
      r_decoded_address__7 <= data__1[7];
      //
      r_decoded_address__8 <= data__2[0];
      r_decoded_address__9 <= data__2[1];
      r_decoded_address_10 <= data__2[2];
      r_decoded_address_11 <= data__2[3];
      r_decoded_address_12 <= data__2[4];
      r_decoded_address_13 <= data__2[5];
      r_decoded_address_14 <= data__2[6];
      r_decoded_address_15 <= data__2[7];
      //
      r_decoded_address_16 <= data__3[0];
      r_decoded_address_17 <= data__3[1];
      r_decoded_address_18 <= data__3[2];
      r_decoded_address_19 <= data__3[3];
      r_decoded_address_20 <= data__3[4];
      r_decoded_address_21 <= data__3[5];
      r_decoded_address_22 <= data__3[6];
      r_decoded_address_23 <= data__3[7];
      //
      r_decoded_address_24 <= data__4[0];
      r_decoded_address_25 <= data__4[1];
      r_decoded_address_26 <= data__4[2];
      r_decoded_address_27 <= data__4[3];
      r_decoded_address_28 <= data__4[4];
      r_decoded_address_29 <= data__4[5];
      r_decoded_address_30 <= data__4[6];
      r_decoded_address_31 <= data__4[7];

      // information byte:
      r_hyp          <= data__5[1];
      r_altis        <= data__5[2];
      r_ns           <= data__5[3];
      r_isync_reason <= data__5[6:5];

      // cyclecount, 0-5 bytes:
      if ( data__5[6:5] != 2'b00 ) begin
        updateCyclecount(data__6, data__7, data__8, data__9, data_10);
      end

      // contextid:
      if ( data__5[6:5] == 2'b00 ) begin
        updateContextid(data__6, data__7, data__8, data__9);
      end
      else if ( data__6[6] == 1'b0 ) begin
        updateContextid(data__7, data__8, data__9, data_10);
      end
      else if ( data__7[7] == 1'b0 ) begin
        updateContextid(data__8, data__9, data_10, data_11);
      end
      else if ( data__8[7] == 1'b0 ) begin
        updateContextid(data__9, data_10, data_11, data_12);
      end
      else if ( data__9[7] == 1'b0 ) begin
        updateContextid(data_10, data_11, data_12, data_13);
      end
      else begin
        updateContextid(data_11, data_12, data_13, data_14);
      end

    end
    else if ( packet_ready && (available_packet == p_atom) ) begin

      /**************************
       ** decode atom packets: **
       **************************/

      r_atom_f <= data__0[1];
      updateCyclecount(data__0, data__1, data__2, data__3, data__4);

    end
    else if ( packet_ready && (available_packet == p_branch) ) begin

      /****************************
       ** decode branch packets: **
       ****************************/

      /*
       * instruction set state changed?
       */
      if ( data__0[7] && data__1[7] && data__2[7] && data__3[7] ) begin
        // WARNING: blocking assignment, r_decoded_isetstate is ONLY used later
        // in the same block
        r_decoded_isetstate = (data__4[5]) ? is_jazelle :
                              (data__4[4]) ? is_thumb   :
                              (data__4[3]) ? is_arm     : r_decoded_isetstate;
      end

      /*
       * update address:
       */
      case ( r_decoded_isetstate )
        is_arm : begin
          r_decoded_address__0 <= 1'b0;
          r_decoded_address__1 <= 1'b0;
          r_decoded_address__2 <= data__0[1];
          r_decoded_address__3 <= data__0[2];
          r_decoded_address__4 <= data__0[3];
          r_decoded_address__5 <= data__0[4];
          r_decoded_address__6 <= data__0[5];
          r_decoded_address__7 <= data__0[6];
          if ( data__0[7] ) begin
            r_decoded_address__8 <= data__1[0];
            r_decoded_address__9 <= data__1[1];
            r_decoded_address_10 <= data__1[2];
            r_decoded_address_11 <= data__1[3];
            r_decoded_address_12 <= data__1[4];
            r_decoded_address_13 <= data__1[5];
            if ( data__1[7] ) begin
              r_decoded_address_14 <= data__1[6];
              r_decoded_address_15 <= data__2[0];
              r_decoded_address_16 <= data__2[1];
              r_decoded_address_17 <= data__2[2];
              r_decoded_address_18 <= data__2[3];
              r_decoded_address_19 <= data__2[4];
              r_decoded_address_20 <= data__2[5];
              if ( data__2[7] ) begin
                r_decoded_address_21 <= data__2[6];
                r_decoded_address_22 <= data__3[0];
                r_decoded_address_23 <= data__3[1];
                r_decoded_address_24 <= data__3[2];
                r_decoded_address_25 <= data__3[3];
                r_decoded_address_26 <= data__3[4];
                r_decoded_address_27 <= data__3[5];
                if ( data__3[7] ) begin
                  r_decoded_address_28 <= data__3[6];
                  r_decoded_address_29 <= data__4[0];
                  r_decoded_address_30 <= data__4[1];
                  r_decoded_address_31 <= data__4[2];
                end
              end
            end
          end
        end
        //
        is_thumb : begin
          r_decoded_address__0 <= 1'b0;
          r_decoded_address__1 <= data__0[1];
          r_decoded_address__2 <= data__0[2];
          r_decoded_address__3 <= data__0[3];
          r_decoded_address__4 <= data__0[4];
          r_decoded_address__5 <= data__0[5];
          r_decoded_address__6 <= data__0[6];
          if ( data__0[7] ) begin
            r_decoded_address__7 <= data__1[0];
            r_decoded_address__8 <= data__1[1];
            r_decoded_address__9 <= data__1[2];
            r_decoded_address_10 <= data__1[3];
            r_decoded_address_11 <= data__1[4];
            r_decoded_address_12 <= data__1[5];
            if ( data__1[7] ) begin
              r_decoded_address_13 <= data__1[6];
              r_decoded_address_14 <= data__2[0];
              r_decoded_address_15 <= data__2[1];
              r_decoded_address_16 <= data__2[2];
              r_decoded_address_17 <= data__2[3];
              r_decoded_address_18 <= data__2[4];
              r_decoded_address_19 <= data__2[5];
              if ( data__2[7] ) begin
                r_decoded_address_20 <= data__2[6];
                r_decoded_address_21 <= data__3[0];
                r_decoded_address_22 <= data__3[1];
                r_decoded_address_23 <= data__3[2];
                r_decoded_address_24 <= data__3[3];
                r_decoded_address_25 <= data__3[4];
                r_decoded_address_26 <= data__3[5];
                if ( data__3[7] ) begin
                  r_decoded_address_27 <= data__3[6];
                  r_decoded_address_28 <= data__4[0];
                  r_decoded_address_29 <= data__4[1];
                  r_decoded_address_30 <= data__4[2];
                  r_decoded_address_31 <= data__4[3];
                end
              end
            end
          end
        end
        //
        is_jazelle : begin
          r_decoded_address__0 <= data__0[1];
          r_decoded_address__1 <= data__0[2];
          r_decoded_address__2 <= data__0[3];
          r_decoded_address__3 <= data__0[4];
          r_decoded_address__4 <= data__0[5];
          r_decoded_address__5 <= data__0[6];
          if ( data__0[7] ) begin
            r_decoded_address__6 <= data__1[0];
            r_decoded_address__7 <= data__1[1];
            r_decoded_address__8 <= data__1[2];
            r_decoded_address__9 <= data__1[3];
            r_decoded_address_10 <= data__1[4];
            r_decoded_address_11 <= data__1[5];
            if ( data__1[7] ) begin
              r_decoded_address_12 <= data__1[6];
              r_decoded_address_13 <= data__2[0];
              r_decoded_address_14 <= data__2[1];
              r_decoded_address_15 <= data__2[2];
              r_decoded_address_16 <= data__2[3];
              r_decoded_address_17 <= data__2[4];
              r_decoded_address_18 <= data__2[5];
              if ( data__2[7] ) begin
                r_decoded_address_19 <= data__2[6];
                r_decoded_address_20 <= data__3[0];
                r_decoded_address_21 <= data__3[1];
                r_decoded_address_22 <= data__3[2];
                r_decoded_address_23 <= data__3[3];
                r_decoded_address_24 <= data__3[4];
                r_decoded_address_25 <= data__3[5];
                if ( data__3[7] ) begin
                  r_decoded_address_26 <= data__3[6];
                  r_decoded_address_27 <= data__4[0];
                  r_decoded_address_28 <= data__4[1];
                  r_decoded_address_29 <= data__4[2];
                  r_decoded_address_30 <= data__4[3];
                  r_decoded_address_31 <= data__4[4];
                end
              end
            end
          end
        end
        //
        default : begin
          // nothing
        end
      endcase

      /*
       * update exception:
       * (If a packet requires an exception information byte, the PTM must
       * generate at least two address bytes. source: ARM Coresight PFT
       * architecture specification)
       */
      r_ns                     <= 1'b0;
      r_decoded_exception[3:0] <= 4'b0000;
      r_altis                  <= 1'b0;
      r_decoded_exception[8:4] <= 5'b00000;
      r_hyp                    <= 1'b0;
      //
      if ( data__0[7] ) begin
        // case of two adress bytes:
        if ( data__1[7] == 1'b0 && data__1[6] == 1'b1 ) begin
          r_ns                     <= data__2[0];
          r_decoded_exception[3:0] <= data__2[4:1];
          r_altis                  <= data__2[6];
          if ( data__2[7] ) begin
            r_decoded_exception[8:4] <= data__3[4:0];
            r_hyp                    <= data__3[5];
          end
        end
        // case of three adress bytes:
        if ( data__1[7] == 1'b1 &&
             data__2[7] == 1'b0 && data__2[6] == 1'b1 ) begin
          r_ns                     <= data__3[0];
          r_decoded_exception[3:0] <= data__3[4:1];
          r_altis                  <= data__3[6];
          if ( data__3[7] ) begin
            r_decoded_exception[8:4] <= data__4[4:0];
            r_hyp                    <= data__4[5];
          end
        end
        // case of four adress bytes:
        if ( data__1[7] == 1'b1 &&
             data__2[7] == 1'b1 &&
             data__3[7] == 1'b0 && data__3[6] == 1'b1 ) begin
          r_ns                     <= data__4[0];
          r_decoded_exception[3:0] <= data__4[4:1];
          r_altis                  <= data__4[6];
          if ( data__4[7] ) begin
            r_decoded_exception[8:4] <= data__5[4:0];
            r_hyp                    <= data__5[5];
          end
        end
        // case of five adress bytes:
        if ( data__1[7] == 1'b1 &&
             data__2[7] == 1'b1 &&
             data__3[7] == 1'b1 &&
                                   data__4[6] == 1'b1 ) begin
          r_ns                     <= data__5[0];
          r_decoded_exception[3:0] <= data__5[4:1];
          r_altis                  <= data__5[6];
          if ( data__5[7] ) begin
            r_decoded_exception[8:4] <= data__6[4:0];
            r_hyp                    <= data__6[5];
          end
        end
      end

      /*
       * update cyclecount:
       */
      // case of one adress byte and no exception byte:
      if ( data__0[7] == 1'b0 ) begin
        updateCyclecount(data__1, data__2, data__3, data__4, data__5);
      end
      // case of two adress bytes:
      if ( data__0[7] == 1'b1 &&
           data__1[7] == 1'b0 ) begin
        // no exception byte:
        if ( data__1[6] == 1'b0 ) begin
          updateCyclecount(data__2, data__3, data__4, data__5, data__6);
        end
        // one exception byte:
        if ( data__1[6] == 1'b1 && data__2[7] == 1'b0 ) begin
          updateCyclecount(data__3, data__4, data__5, data__6, data__7);
        end
        // two exception bytes:
        if ( data__1[6] == 1'b1 && data__2[7] == 1'b1 ) begin
          updateCyclecount(data__4, data__5, data__6, data__7, data__8);
        end
      end
      // case of three adress bytes:
      if ( data__0[7] == 1'b1 &&
           data__1[7] == 1'b1 &&
           data__2[7] == 1'b0 ) begin
        // no exception byte:
        if ( data__2[6] == 1'b0 ) begin
          updateCyclecount(data__3, data__4, data__5, data__6, data__7);
        end
        // one exception byte:
        if ( data__2[6] == 1'b1 && data__3[7] == 1'b0 ) begin
          updateCyclecount(data__4, data__5, data__6, data__7, data__8);
        end
        // two exception bytes:
        if ( data__2[6] == 1'b1 && data__3[7] == 1'b1 ) begin
          updateCyclecount(data__5, data__6, data__7, data__8, data__9);
        end
      end
      // case of four adress bytes:
      if ( data__0[7] == 1'b1 &&
           data__1[7] == 1'b1 &&
           data__2[7] == 1'b1 &&
           data__3[7] == 1'b0 ) begin
        // no exception byte:
        if ( data__3[6] == 1'b0 ) begin
          updateCyclecount(data__4, data__5, data__6, data__7, data__8);
        end
        // one exception byte:
        if ( data__3[6] == 1'b1 && data__4[7] == 1'b0 ) begin
          updateCyclecount(data__5, data__6, data__7, data__8, data__9);
        end
        // two exception bytes:
        if ( data__3[6] == 1'b1 && data__4[7] == 1'b1 ) begin
          updateCyclecount(data__6, data__7, data__8, data__9, data_10);
        end
      end
      // case of five adress bytes and no exception byte:
      if ( data__0[7] == 1'b1 &&
           data__1[7] == 1'b1 &&
           data__2[7] == 1'b1 &&
           data__3[7] == 1'b1 ) begin
        // no exception byte:
        if ( data__4[6] == 1'b0 ) begin
          updateCyclecount(data__5, data__6, data__7, data__8, data__9);
        end
        // one exception byte:
        if ( data__4[6] == 1'b1 && data__5[7] == 1'b0 ) begin
          updateCyclecount(data__6, data__7, data__8, data__9, data_10);
        end
        // two exception bytes:
        if ( data__4[6] == 1'b1 && data__5[7] == 1'b1 ) begin
          updateCyclecount(data__7, data__8, data__9, data_10, data_11);
        end
      end

    end
  end

  /****************************************************************************/

  assign decoded_isetstate  = (
    ( !nrst ) ?
      is_unknown
    :
    ( nrst & packet_ready & (available_packet == p_isync) & !data__1[0] ) ?
      is_arm
    :
    ( nrst & packet_ready & (available_packet == p_isync) &  data__1[0] ) ?
      is_thumb
    :
    ( nrst & packet_ready & (available_packet == p_branch) & data__0[7] &
                                                             data__1[7] &
                                                             data__2[7] &
                                                             data__3[7] &
                                                             data__4[5:3] == 3'b001 ) ?
      is_arm
    :
    ( nrst & packet_ready & (available_packet == p_branch) & data__0[7] &
                                                             data__1[7] &
                                                             data__2[7] &
                                                             data__3[7] &
                                                             data__4[5:4] == 2'b01 ) ?
      is_thumb
    :
    ( nrst & packet_ready & (available_packet == p_branch) & data__0[7] &
                                                             data__1[7] &
                                                             data__2[7] &
                                                             data__3[7] &
                                                             data__4[5] == 1'b1 ) ?
      is_jazelle
    :
    r_decoded_isetstate
  );

  //
  assign decoded_address__0 = (
      ( !nrst )                                                                                    ? 1'b0
    : ( nrst & packet_ready & (available_packet == p_isync) )                                      ? 1'b0
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_arm) )     ? 1'b0
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_thumb) )   ? 1'b0
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_jazelle) ) ? data__0[1]
    : r_decoded_address__0
  );
  assign decoded_address__1 = (
      ( !nrst )                                                                                    ? 1'b0
    : ( nrst & packet_ready & (available_packet == p_isync) )                                      ? data__1[1]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_arm) )     ? 1'b0
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_thumb) )   ? data__0[1]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_jazelle) ) ? data__0[2]
    : r_decoded_address__1
  );
  assign decoded_address__2 = (
      ( !nrst )                                                                                    ? 1'b0
    : ( nrst & packet_ready & (available_packet == p_isync) )                                      ? data__1[2]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_arm) )     ? data__0[1]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_thumb) )   ? data__0[2]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_jazelle) ) ? data__0[3]
    : r_decoded_address__2
  );
  assign decoded_address__3 = (
      ( !nrst )                                                                                    ? 1'b0
    : ( nrst & packet_ready & (available_packet == p_isync) )                                      ? data__1[3]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_arm) )     ? data__0[2]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_thumb) )   ? data__0[3]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_jazelle) ) ? data__0[4]
    : r_decoded_address__3
  );
  assign decoded_address__4 = (
      ( !nrst )                                                                                    ? 1'b0
    : ( nrst & packet_ready & (available_packet == p_isync) )                                      ? data__1[4]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_arm) )     ? data__0[3]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_thumb) )   ? data__0[4]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_jazelle) ) ? data__0[5]
    : r_decoded_address__4
  );
  assign decoded_address__5 = (
      ( !nrst )                                                                                    ? 1'b0
    : ( nrst & packet_ready & (available_packet == p_isync) )                                      ? data__1[5]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_arm) )     ? data__0[4]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_thumb) )   ? data__0[5]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_jazelle) ) ? data__0[6]
    : r_decoded_address__5
  );
  assign decoded_address__6 = (
      ( !nrst )                                                                                                 ? 1'b0
    : ( nrst & packet_ready & (available_packet == p_isync) )                                                   ? data__1[6]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_arm) )                  ? data__0[5]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_thumb) )                ? data__0[6]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_jazelle) & data__0[7] ) ? data__1[0]
    : r_decoded_address__6
  );
  assign decoded_address__7 = (
      ( !nrst )                                                                                                 ? 1'b0
    : ( nrst & packet_ready & (available_packet == p_isync) )                                                   ? data__1[7]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_arm) )                  ? data__0[6]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_thumb)   & data__0[7] ) ? data__1[0]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_jazelle) & data__0[7] ) ? data__1[1]
    : r_decoded_address__7
  );
  assign decoded_address__8 = (
      ( !nrst )                                                                                                 ? 1'b0
    : ( nrst & packet_ready & (available_packet == p_isync) )                                                   ? data__2[0]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_arm)     & data__0[7] ) ? data__1[0]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_thumb)   & data__0[7] ) ? data__1[1]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_jazelle) & data__0[7] ) ? data__1[2]
    : r_decoded_address__8
  );
  assign decoded_address__9 = (
      ( !nrst )                                                                                                 ? 1'b0
    : ( nrst & packet_ready & (available_packet == p_isync) )                                                   ? data__2[1]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_arm)     & data__0[7] ) ? data__1[1]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_thumb)   & data__0[7] ) ? data__1[2]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_jazelle) & data__0[7] ) ? data__1[3]
    : r_decoded_address__9
  );
  assign decoded_address_10 = (
      ( !nrst )                                                                                                 ? 1'b0
    : ( nrst & packet_ready & (available_packet == p_isync) )                                                   ? data__2[2]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_arm)     & data__0[7] ) ? data__1[2]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_thumb)   & data__0[7] ) ? data__1[3]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_jazelle) & data__0[7] ) ? data__1[4]
    : r_decoded_address_10
  );
  assign decoded_address_11 = (
      ( !nrst )                                                                                                 ? 1'b0
    : ( nrst & packet_ready & (available_packet == p_isync) )                                                   ? data__2[3]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_arm)     & data__0[7] ) ? data__1[3]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_thumb)   & data__0[7] ) ? data__1[4]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_jazelle) & data__0[7] ) ? data__1[5]
    : r_decoded_address_11
  );
  assign decoded_address_12 = (
      ( !nrst )                                                                                                              ? 1'b0
    : ( nrst & packet_ready & (available_packet == p_isync) )                                                                ? data__2[4]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_arm)     & data__0[7] )              ? data__1[4]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_thumb)   & data__0[7] )              ? data__1[5]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_jazelle) & data__0[7] & data__1[7] ) ? data__1[6]
    : r_decoded_address_12
  );
  assign decoded_address_13 = (
      ( !nrst )                                                                                                              ? 1'b0
    : ( nrst & packet_ready & (available_packet == p_isync) )                                                                ? data__2[5]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_arm)     & data__0[7] )              ? data__1[5]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_thumb)   & data__0[7] & data__1[7] ) ? data__1[6]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_jazelle) & data__0[7] & data__1[7] ) ? data__2[0]
    : r_decoded_address_13
  );
  assign decoded_address_14 = (
      ( !nrst )                                                                                                              ? 1'b0
    : ( nrst & packet_ready & (available_packet == p_isync) )                                                                ? data__2[6]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_arm)     & data__0[7] & data__1[7] ) ? data__1[6]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_thumb)   & data__0[7] & data__1[7] ) ? data__2[0]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_jazelle) & data__0[7] & data__1[7] ) ? data__2[1]
    : r_decoded_address_14
  );
  assign decoded_address_15 = (
      ( !nrst )                                                                                                              ? 1'b0
    : ( nrst & packet_ready & (available_packet == p_isync) )                                                                ? data__2[7]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_arm)     & data__0[7] & data__1[7] ) ? data__2[0]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_thumb)   & data__0[7] & data__1[7] ) ? data__2[1]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_jazelle) & data__0[7] & data__1[7] ) ? data__2[2]
    : r_decoded_address_15
  );
  assign decoded_address_16 = (
      ( !nrst )                                                                                                              ? 1'b0
    : ( nrst & packet_ready & (available_packet == p_isync) )                                                                ? data__3[0]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_arm)     & data__0[7] & data__1[7] ) ? data__2[1]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_thumb)   & data__0[7] & data__1[7] ) ? data__2[2]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_jazelle) & data__0[7] & data__1[7] ) ? data__2[3]
    : r_decoded_address_16
  );
  assign decoded_address_17 = (
      ( !nrst )                                                                                                              ? 1'b0
    : ( nrst & packet_ready & (available_packet == p_isync) )                                                                ? data__3[1]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_arm)     & data__0[7] & data__1[7] ) ? data__2[2]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_thumb)   & data__0[7] & data__1[7] ) ? data__2[3]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_jazelle) & data__0[7] & data__1[7] ) ? data__2[4]
    : r_decoded_address_17
  );
  assign decoded_address_18 = (
      ( !nrst )                                                                                                              ? 1'b0
    : ( nrst & packet_ready & (available_packet == p_isync) )                                                                ? data__3[2]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_arm)     & data__0[7] & data__1[7] ) ? data__2[3]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_thumb)   & data__0[7] & data__1[7] ) ? data__2[4]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_jazelle) & data__0[7] & data__1[7] ) ? data__2[5]
    : r_decoded_address_18
  );
  assign decoded_address_19 = (
      ( !nrst )                                                                                                                           ? 1'b0
    : ( nrst & packet_ready & (available_packet == p_isync) )                                                                             ? data__3[3]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_arm)     & data__0[7] & data__1[7] )              ? data__2[4]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_thumb)   & data__0[7] & data__1[7] )              ? data__2[5]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_jazelle) & data__0[7] & data__1[7] & data__2[7] ) ? data__2[6]
    : r_decoded_address_19
  );
  assign decoded_address_20 = (
      ( !nrst )                                                                                                                           ? 1'b0
    : ( nrst & packet_ready & (available_packet == p_isync) )                                                                             ? data__3[4]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_arm)     & data__0[7] & data__1[7] )              ? data__2[5]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_thumb)   & data__0[7] & data__1[7] & data__2[7] ) ? data__2[6]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_jazelle) & data__0[7] & data__1[7] & data__2[7] ) ? data__3[0]
    : r_decoded_address_20
  );
  assign decoded_address_21 = (
      ( !nrst )                                                                                                                           ? 1'b0
    : ( nrst & packet_ready & (available_packet == p_isync) )                                                                             ? data__3[5]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_arm)     & data__0[7] & data__1[7] & data__2[7] ) ? data__2[6]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_thumb)   & data__0[7] & data__1[7] & data__2[7] ) ? data__3[0]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_jazelle) & data__0[7] & data__1[7] & data__2[7] ) ? data__3[1]
    : r_decoded_address_21
  );
  assign decoded_address_22 = (
      ( !nrst )                                                                                                                           ? 1'b0
    : ( nrst & packet_ready & (available_packet == p_isync) )                                                                             ? data__3[6]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_arm)     & data__0[7] & data__1[7] & data__2[7] ) ? data__3[0]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_thumb)   & data__0[7] & data__1[7] & data__2[7] ) ? data__3[1]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_jazelle) & data__0[7] & data__1[7] & data__2[7] ) ? data__3[2]
    : r_decoded_address_22
  );
  assign decoded_address_23 = (
      ( !nrst )                                                                                                                           ? 1'b0
    : ( nrst & packet_ready & (available_packet == p_isync) )                                                                             ? data__3[7]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_arm)     & data__0[7] & data__1[7] & data__2[7] ) ? data__3[1]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_thumb)   & data__0[7] & data__1[7] & data__2[7] ) ? data__3[2]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_jazelle) & data__0[7] & data__1[7] & data__2[7] ) ? data__3[3]
    : r_decoded_address_23
  );
  assign decoded_address_24 = (
      ( !nrst )                                                                                                                           ? 1'b0
    : ( nrst & packet_ready & (available_packet == p_isync) )                                                                             ? data__4[0]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_arm)     & data__0[7] & data__1[7] & data__2[7] ) ? data__3[2]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_thumb)   & data__0[7] & data__1[7] & data__2[7] ) ? data__3[3]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_jazelle) & data__0[7] & data__1[7] & data__2[7] ) ? data__3[4]
    : r_decoded_address_24
  );
  assign decoded_address_25 = (
      ( !nrst )                                                                                                                           ? 1'b0
    : ( nrst & packet_ready & (available_packet == p_isync) )                                                                             ? data__4[1]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_arm)     & data__0[7] & data__1[7] & data__2[7] ) ? data__3[3]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_thumb)   & data__0[7] & data__1[7] & data__2[7] ) ? data__3[4]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_jazelle) & data__0[7] & data__1[7] & data__2[7] ) ? data__3[5]
    : r_decoded_address_25
  );
  assign decoded_address_26 = (
      ( !nrst )                                                                                                                                        ? 1'b0
    : ( nrst & packet_ready & (available_packet == p_isync) )                                                                                          ? data__4[2]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_arm)     & data__0[7] & data__1[7] & data__2[7] )              ? data__3[4]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_thumb)   & data__0[7] & data__1[7] & data__2[7] )              ? data__3[5]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_jazelle) & data__0[7] & data__1[7] & data__2[7] & data__3[7] ) ? data__3[6]
    : r_decoded_address_26
  );
  assign decoded_address_27 = (
      ( !nrst )                                                                                                                                        ? 1'b0
    : ( nrst & packet_ready & (available_packet == p_isync) )                                                                                          ? data__4[3]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_arm)     & data__0[7] & data__1[7] & data__2[7] )              ? data__3[5]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_thumb)   & data__0[7] & data__1[7] & data__2[7] & data__3[7] ) ? data__3[6]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_jazelle) & data__0[7] & data__1[7] & data__2[7] & data__3[7] ) ? data__4[0]
    : r_decoded_address_27
  );
  assign decoded_address_28 = (
      ( !nrst )                                                                                                                                        ? 1'b0
    : ( nrst & packet_ready & (available_packet == p_isync) )                                                                                          ? data__4[4]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_arm)     & data__0[7] & data__1[7] & data__2[7] & data__3[7] ) ? data__3[6]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_thumb)   & data__0[7] & data__1[7] & data__2[7] & data__3[7] ) ? data__4[0]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_jazelle) & data__0[7] & data__1[7] & data__2[7] & data__3[7] ) ? data__4[1]
    : r_decoded_address_28
  );
  assign decoded_address_29 = (
      ( !nrst )                                                                                                                                        ? 1'b0
    : ( nrst & packet_ready & (available_packet == p_isync) )                                                                                          ? data__4[5]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_arm)     & data__0[7] & data__1[7] & data__2[7] & data__3[7] ) ? data__4[0]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_thumb)   & data__0[7] & data__1[7] & data__2[7] & data__3[7] ) ? data__4[1]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_jazelle) & data__0[7] & data__1[7] & data__2[7] & data__3[7] ) ? data__4[2]
    : r_decoded_address_29
  );
  assign decoded_address_30 = (
      ( !nrst )                                                                                                                                        ? 1'b0
    : ( nrst & packet_ready & (available_packet == p_isync) )                                                                                          ? data__4[6]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_arm)     & data__0[7] & data__1[7] & data__2[7] & data__3[7] ) ? data__4[1]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_thumb)   & data__0[7] & data__1[7] & data__2[7] & data__3[7] ) ? data__4[2]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_jazelle) & data__0[7] & data__1[7] & data__2[7] & data__3[7] ) ? data__4[3]
    : r_decoded_address_30
  );
  assign decoded_address_31 = (
      ( !nrst )                                                                                                                                        ? 1'b0
    : ( nrst & packet_ready & (available_packet == p_isync) )                                                                                          ? data__4[7]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_arm)     & data__0[7] & data__1[7] & data__2[7] & data__3[7] ) ? data__4[2]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_thumb)   & data__0[7] & data__1[7] & data__2[7] & data__3[7] ) ? data__4[3]
    : ( nrst & packet_ready & (available_packet == p_branch) & (decoded_isetstate == is_jazelle) & data__0[7] & data__1[7] & data__2[7] & data__3[7] ) ? data__4[4]
    : r_decoded_address_31
  );
  //
  assign hyp          = r_hyp;
  assign ns           = r_ns;
  assign altis        = r_altis;
  assign isync_reason = r_isync_reason;
  //
  assign cyclecount__0 = r_cyclecount__0;
  assign cyclecount__1 = r_cyclecount__1;
  assign cyclecount__2 = r_cyclecount__2;
  assign cyclecount__3 = r_cyclecount__3;
  assign cyclecount__4 = r_cyclecount__4;
  assign cyclecount__5 = r_cyclecount__5;
  assign cyclecount__6 = r_cyclecount__6;
  assign cyclecount__7 = r_cyclecount__7;
  assign cyclecount__8 = r_cyclecount__8;
  assign cyclecount__9 = r_cyclecount__9;
  assign cyclecount_10 = r_cyclecount_10;
  assign cyclecount_11 = r_cyclecount_11;
  assign cyclecount_12 = r_cyclecount_12;
  assign cyclecount_13 = r_cyclecount_13;
  assign cyclecount_14 = r_cyclecount_14;
  assign cyclecount_15 = r_cyclecount_15;
  assign cyclecount_16 = r_cyclecount_16;
  assign cyclecount_17 = r_cyclecount_17;
  assign cyclecount_18 = r_cyclecount_18;
  assign cyclecount_19 = r_cyclecount_19;
  assign cyclecount_20 = r_cyclecount_20;
  assign cyclecount_21 = r_cyclecount_21;
  assign cyclecount_22 = r_cyclecount_22;
  assign cyclecount_23 = r_cyclecount_23;
  assign cyclecount_24 = r_cyclecount_24;
  assign cyclecount_25 = r_cyclecount_25;
  assign cyclecount_26 = r_cyclecount_26;
  assign cyclecount_27 = r_cyclecount_27;
  assign cyclecount_28 = r_cyclecount_28;
  assign cyclecount_29 = r_cyclecount_29;
  assign cyclecount_30 = r_cyclecount_30;
  assign cyclecount_31 = r_cyclecount_31;
  //
  assign contextid_0 = r_contextid_0;
  assign contextid_1 = r_contextid_1;
  assign contextid_2 = r_contextid_2;
  assign contextid_3 = r_contextid_3;
  //
  assign atom_f            = r_atom_f;
  assign decoded_exception = r_decoded_exception;

endmodule

