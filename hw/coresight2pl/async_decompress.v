/*
 * Copyright (C) 2021 Jonathan Certes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

/**
 * \brief
 *  Finite state machine to decode a A-sync alignment synchronization packet.
 *
 * \details
 *  Output busy rises when header (trace_data == 8'h00) is received. It stays up
 *  until ready is set or a reset is triggered. It falls when ready and not
 *  receiving a header (trace_data == 8'h00) in the next state. It also falls
 *  when a reset is triggered.
 *
 *  The decoding is ready once it receives a sequence of four or more 8'h00
 *  followed by one 8'h80. It is not ready when it receives 8'h00. Having a
 *  reset makes it not ready.
 *
 *  More informations in ARM Coresight PFT architecture specification ;
 *  subsection A-sync, alignment synchronization packet.
 */
module async_decompress
(
  input       nrst,       //!< low level active reset
  input       clk,        //!< clock
  input [7:0] trace_data, //!< Byte-aligned trace data
  input       trace_ctl,  //!< active when 0
  //
  output busy,  //!< indicates that the decoding is ongoing
  output ready  //!< packet boundary alignment has terminated
);

  localparam [1:0] state_start  = 2'b00;
  localparam [1:0] state_header = 2'b01;
  localparam [1:0] state_end    = 2'b10;
  reg [1:0] state;
  reg [2:0] count;
  //
  always @( posedge(clk) ) begin
    if ( nrst == 1'b0 ) begin
      count <= 0;
      state <= state_start;
    end
    else begin
      if ( trace_ctl == 1'b0 ) begin
        case ( state )
          state_start : begin
            if ( trace_data == 8'h00 ) begin
              state <= state_header;
              count <= 1;
            end
            else begin
              state <= state_start;
              count <= 0;
            end
          end

          state_header : begin
            if ( trace_data == 8'h00 ) begin
              state <= state_header;
              if ( count < 4 ) begin
                count <= count + 1;
              end
              else begin
                count <= 4;
              end
            end
            else if ( trace_data == 8'h80 && count == 4 ) begin
              // here it must be ready!
              state <= state_end;
              count <= 0;
            end
            else begin
              state <= state_start;
              count <= 0;
            end
          end

          state_end : begin
            if ( trace_data == 8'h00 ) begin
              state <= state_header;
              count <= 1;
            end
            else begin
              // here it must be ready!
              state <= state_end;
              count <= 1;
            end
          end

          default : begin
            if ( trace_data == 8'h00 ) begin
              state <= state_header;
              count <= 1;
            end
            else begin
              state <= state_start;
              count <= 0;
            end
          end
        endcase
      end
    end
  end

  assign busy = (nrst == 1'b1) && (
       (state == state_start) && (trace_ctl == 1'b0 && trace_data == 8'h00)
    || (state == state_header)
    || (state == state_end)   && (trace_ctl == 1'b0 && trace_data == 8'h00)
    // handling incorrect behaviour:
    || (state != state_start) && (state != state_header) && (state != state_end)
  );

  assign ready = (nrst == 1'b1) && (
         (state      == state_header)
      && (trace_ctl  == 1'b0)
      && (trace_data == 8'h80 && count == 4)
    ||
         (state      == state_end)
      && (trace_ctl  == 1'b0)
      && (trace_data != 8'h00)
  );

endmodule

